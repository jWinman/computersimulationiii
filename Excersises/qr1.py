#!/usr/bin/python 
import numpy as np
#from scipy import linalg
A = np.array([[0, 1], [1, 1], [1, 1],[2, 1]])
b = np.array([1, 0, 2, 1])
Q, R = np.linalg.qr(A)
p = np.dot(Q.T, b)
x = np.dot(np.linalg.inv(R), p)
np.set_printoptions(precision=3)
print('\n')
print(A)
print('\n')
print(Q)
print('\n')
print(R)
print('\n')
print(x)
