#!/usr/bin/python 
import numpy as np
a = np.array([[2,1,1], [4,-6,0], [-2, 7, 2]])
b = np.array([[1, 3, 3, 2], [2, 6, 9, 5], [-1, -3, 3, 0]])
q = np.linalg.matrix_rank(a)
r = np.linalg.matrix_rank(b)
print(q, r)
