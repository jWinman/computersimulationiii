#!/usr/bin/python 
"""
       DGETRF computes an LU factorization of a general M-by-N matrix A using partial pivoting with row interchanges.  The factorization has the form
       A = P * L * U
       where  P  is  a  permutation  matrix,  L is lower triangular with unit diagonal elements (lower trapezoidal if m > n), and U is upper triangular
      (upper trapezoidal if m < n).
"""
import numpy as np
from scipy import linalg
a = np.array([[2,1,1], [4,2,1.999], [-2, 7, 2]])
b = linalg.lapack.dgetrf(a)
print('\n')
print("b")
print(b)
