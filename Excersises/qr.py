import numpy as np
from scipy import linalg
from scipy.stats import threshold

a = np.array([[2,1,1,1], [1,3,0,0], [1,0,1,0],[1,0,0,1]])
Q, R = linalg.qr(a)
an = np.zeros(np.shape(a))
Qn = Q

for _ in range(45):
    an = R @ Q
    Q, R = linalg.qr(an)
    Qn = Qn @ Q

print(threshold(Qn @ an @ Qn.T, 1e-10))
print(threshold(a, 1e-10))

quit()
np.set_printoptions(precision=3)
print('\n')
print(an)
print('\n')
print(Q)
print('\n')
print(R)
