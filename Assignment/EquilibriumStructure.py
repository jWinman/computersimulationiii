import functools as ft
import numpy as np
import numdifftools as nd
import itertools as it
import scipy.spatial.distance as ssd
from scipy import optimize

#Parameter
rho = 1e4
lambd = 100

# Interactions
# Potentials
coulomb = lambda diff, q: q / diff
repulsive = lambda diff: rho * np.exp(-diff / lambd)

# Forces
coulombForce = lambda differencesVector, charge: - charge / np.linalg.norm(differencesVector)**3 * differencesVector
repulsiveForce = lambda differencesVector: - rho / lambd * np.exp(- np.linalg.norm(differencesVector) / lambd) * differencesVector / np.linalg.norm(differencesVector)

# Hessians
def coloumbHessianDiag(differencesVector, charge):
    # returns the diagonal 3x3 matrices in the NxN matrix
    r = np.linalg.norm(differencesVector)
    H = np.zeros((len(differencesVector), len(differencesVector)))
    H[0][0] = - charge / r**3 + 3 * charge * differencesVector[0]**2 / r**5
    if (len(differencesVector) >= 2):
        H[1][1] = - charge / r**3 + 3 * charge * differencesVector[1]**2 / r**5
        H[0][1] = H[1][0] = 3 * charge * differencesVector[0] * differencesVector[1] / r**5

    if (len(differencesVector) >= 3):
        H[2][2] = - charge / r**3 + 3 * charge * differencesVector[2]**2 / r**5
        H[0][2] = H[2][0] = 3 * charge * differencesVector[0] * differencesVector[2] / r**5
        H[1][2] = H[2][1] = 3 * charge * differencesVector[1] * differencesVector[2] / r**5

    return H

def repulsiveHessianDiag(differencesVector):
    # returns the diagonal 3x3 matrices in the NxN matrix
    r = np.linalg.norm(differencesVector)
    H = np.zeros((len(differencesVector), len(differencesVector)))

    H[0][0] = - rho / lambd * np.exp(- r / lambd) * (1 / r - differencesVector[0]**2 / r**3 - 1. / lambd * differencesVector[0]**2 / r**2)
    if (len(differencesVector) >= 2):
        H[1][1] = - rho / lambd * np.exp(- r / lambd) * (1 / r - differencesVector[1]**2 / r**3 - 1 / lambd * differencesVector[1]**2 / r**2)
        H[0][1] = H[1][0] = rho / lambd * np.exp(- r / lambd) * differencesVector[0] * differencesVector[1] * (1. / (r**2 * lambd) + 1. / r**3)

    if (len(differencesVector) >= 3):
        H[2][2] = - rho / lambd * np.exp(- r / lambd) * (1 / r - differencesVector[2]**2 / r**3 - 1 / lambd * differencesVector[2]**2 / r**2)
        H[0][2] = H[2][0] = rho / lambd * np.exp(- r / lambd) * differencesVector[0] * differencesVector[2] * (1. / (r**2 * lambd) + 1. / r**3)
        H[1][2] = H[2][1] = rho / lambd * np.exp(- r / lambd) * differencesVector[1] * differencesVector[2] * (1. / (r**2 * lambd) + 1. / r**3)

    return H

def repulsiveHessianOffDiag(differencesVector):
    # returns the off-diagonal 3x3 matrices in the NxN matrix
    r = np.linalg.norm(differencesVector)
    H = np.zeros((len(differencesVector), len(differencesVector)))

    H[0][0] = - rho / lambd * np.exp(- r / lambd) * (-1 / r + differencesVector[0]**2 / r**3 + 1 / lambd * differencesVector[0]**2 / r**2)

    if (len(differencesVector) >= 2):
        H[1][1] = - rho / lambd * np.exp(- r / lambd) * (-1 / r + differencesVector[1]**2 / r**3 + 1 / lambd * differencesVector[1]**2 / r**2)
        H[0][1] = H[1][0] = -rho / lambd * np.exp(- r / lambd) * differencesVector[0] * differencesVector[1] * (1. / (r**2 * lambd) + 1. / r**3)

    if (len(differencesVector) >= 3):
        H[2][2] = -rho / lambd * np.exp(- r / lambd) * (-1 / r + differencesVector[2]**2 / r**3 + 1 / lambd * differencesVector[2]**2 / r**2)
        H[0][2] = H[2][0] = -rho / lambd * np.exp(- r / lambd) * differencesVector[0] * differencesVector[2] * (1. / (r**2 * lambd) + 1. / r**3)
        H[1][2] = H[2][1] = -rho / lambd * np.exp(- r / lambd) * differencesVector[1] * differencesVector[2] * (1. / (r**2 * lambd) + 1. / r**3)

    return H


def coloumbHessianOffDiag(differencesVector, charge):
    # returns the off-diagonal 3x3 matrices in the NxN matrix
    r = np.linalg.norm(differencesVector)
    H = np.zeros((len(differencesVector), len(differencesVector)))

    H[0][0] = charge / r**3 - 3 * charge * (differencesVector[0]**2) / r**5
    if (len(differencesVector) >= 2):
        H[1][1] = charge / r**3 - 3 * charge * (differencesVector[1]**2) / r**5
        H[0][1] = H[1][0] = - 3 * charge * differencesVector[0] * differencesVector[1] / r**5

    if (len(differencesVector) >= 3):
        H[2][2] = charge / r**3 - 3 * charge * (differencesVector[2]**2) / r**5
        H[0][2] = H[2][0] = - 3 * charge * differencesVector[0] * differencesVector[2] / r**5
        H[1][2] = H[1][2] = - 3 * charge * differencesVector[1] * differencesVector[2] / r**5

    return H

# Total Energy and -Force
def PotEnergy(x, q, D):
    """ Calculates the energy and the force vector for given 2 coulomb and repulsive interaction"""
    x = np.reshape(x, (len(x) // D, D))

    # Distances between particles
    distances = ssd.cdist(x, x, metric="euclidean")
    charges = np.reshape(np.kron(q, q), (len(q), len(q)))[distances != 0]
    distances = distances[distances != 0]

    energy = 0.5 * np.sum(coulomb(distances, charges) + repulsive(distances))

    denergies = np.zeros((len(x), D))

    for i in range(len(denergies)):
        for j in range(i):
               denergies[i] += coulombForce(x[i] - x[j], q[i] * q[j])
               denergies[i] += repulsiveForce(x[i] - x[j])
               denergies[j] += coulombForce(x[j] - x[i], q[i] * q[j])
               denergies[j] += repulsiveForce(x[j] - x[i])

    return energy, denergies.flatten()

# Total Hessian
def Hessian(x, q, D):
    """ Calculates the Hessian for given 2 particle interaction"""
    x = np.reshape(x, (len(x) // D, D))

    HNxN = np.zeros((len(x), len(x), D, D))

    for l in range(len(HNxN)):
        for k in range(l + 1):
            for i in range(len(x)):
                for j in range(i):
                    if (l == k and (l == i or l == j)):
                        HNxN[l][k] += coloumbHessianDiag(x[i] - x[j], q[i] * q[j])
                        HNxN[l][k] += repulsiveHessianDiag(x[i] - x[j])
                    if (l == i and k == j and l != k):
                        HNxN[l][k] += coloumbHessianOffDiag(x[i] - x[j], q[i] * q[j])
                        HNxN[k][l] += coloumbHessianOffDiag(x[j] - x[i], q[i] * q[j])

                        HNxN[l][k] += repulsiveHessianOffDiag(x[i] - x[j])
                        HNxN[k][l] += repulsiveHessianOffDiag(x[j] - x[i])

    # reshape the Hessian to a 3N x 3N matrix
    H3Nx3N = np.zeros((D * len(x), D * len(x)))

    A = 0
    for i in range(len(HNxN)):
     for k in range(D):
         for j in range(len(HNxN)):
             for l in range(D):
                 H3Nx3N[A % len(H3Nx3N), A / len(H3Nx3N)] = HNxN[i, j, l, k]
                 A += 1

    return H3Nx3N

# THe BFGS-Algorithm
def BFGS(E, Hessian, x0):
    """
    E: potential function to minimize
    F: gradient, force vector
    H: Hessian matrix
    x0: initial position
    returns: xmin, Emin
    """
    xmin = x0.flatten()
    eps = 1e-4
    Energy = lambda x: E(x)[0]
    Force = lambda x: E(x)[1]

    F0 = Force(xmin)
    H0 = Hessian(xmin)

    Fp = lambda alpha, p: Force(alpha * p)
    while (np.linalg.norm(F0) > eps):
        p = np.linalg.lstsq(H0, F0)[0]
        Fppartial = ft.partial(Fp, p=p)

        alpha = optimize.fsolve(Fppartial, 1.)
        xmin = alpha * p

        F0, H0 = Force(xmin), Hessian(xmin)

    return xmin, Energy(xmin)

# Auxiliary functions
@np.vectorize
def PlotEnergy1D(x1, E):
    x = np.array([0, x1])
    return E(x)[0]

def Energy1D(x1, E):
    x = np.array([0, x1])
    return E(x)[0]

def Denergy1D(x1, E):
    x = np.array([0, x1])
    return E(x)[1][0]

# Test in 1D with 2 Particles
def Test2N1D():
    import matplotlib.pyplot as plt

    # parameters
    N = 2
    q = np.tile([1, -1], int(N / 2))
    D = 1

    # define Energy and Hessian
    E = ft.partial(PotEnergy, q=q, D=D)
    H = ft.partial(Hessian, q=q, D=D)

    # initial positions
    x0 = np.array([0, 1.3])

    # minimize with BFGS
    x, Energy = BFGS(E, H, x0)
    distance = abs(np.diff(x))

    print("1D Problem for 2 Particles: ")
    print("Position of Min., E of Min.", distance, Energy)

    # check by root of derivative
    DE = ft.partial(Denergy1D, E=E)
    distNewton = optimize.newton(DE, 1)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    x = np.linspace(0, 5, 1000)
    ax.plot(x, PlotEnergy1D(x, E), label="Energy")
    ax.plot(distance, Energy, "D", label="Minimum with BFGS")
    ax.plot(distNewton, Energy1D(distNewton, E), "o", label="Root of Newton-Raphson")
    ax.set_ylim(-1, 1)
    ax.set_ylabel(r"$E / \frac{4 \pi \epsilon_0}{e^2 \lambda}$")
    ax.set_xlabel(r"$r / \lambda$")
    ax.legend()
    ax.grid()

    fig.savefig("Plots/1Dtest.pdf")

def Structures3D():
    import matplotlib.pyplot as plt

    Ns = np.array([4]) #np.arange(6, 14, 2)
    qs = np.array([np.tile([1, -1], int(N / 2)) for N in Ns])
    D = 3

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    for i, N in enumerate(Ns):
        q = qs[i]

        E = ft.partial(PotEnergy, q=q, D=D)
        H = ft.partial(Hessian, q=q, D=D)

        x, y, z = np.random.uniform(-N / 2, N / 2, (D, N))
        x = np.arange(0, N, 1)
        y = np.zeros(N)
        z = np.zeros(N)
        #x = np.array([0, 1., 0, 1.])
        #y = np.array([0, 0, 1., 1.])
        #z = np.zeros(4)

        x0 = np.array([x, y, z]).T

        #minimizer_kwargs = {"method": "BFGS", "jac": True}
        #ret = optimize.basinhopping(E, minimizer_kwargs=minimizer_kwargs, x0=x0, niter=1, stepsize=10, T=0.001)
        #xMinimum = np.reshape(ret.x, (len(ret.x) // D, D))
        #print(xMinimum)
        #Energy = ret.fun
        xMinimum, Energy = BFGS(E, H, x0)
        xMinimum = np.reshape(xMinimum, (len(xMinimum) // D, D))
        print(N, Energy)
        print(xMinimum)

        plot(xMinimum, qs, N)

        ax.plot(N, Energy, "b+")

    ax.set_xlim(5, 15)
    ax.grid()
    ax.set_xlabel("$N$")
    ax.set_ylabel(r"$E / \frac{4 \pi \epsilon_0}{e^2 \lambda}$")

    fig.savefig("Plots/Energy.pdf")

def plot(xMinimum, qs, N):
    from mayavi import mlab

    x, y, z = xMinimum.T
    fig = mlab.figure(figure=1, bgcolor=(0, 0, 0), size=(500, 500))
    mlab.points3d(x[::2], y[::2], z[::2], color=(1, 0, 0), resolution=40)
    mlab.points3d(x[1::2], y[1::2], z[1::2], color=(0, 1, 0), resolution=40)
    mlab.show()
    #mlab.savefig("Plots/structure{}.png".format(N))
    mlab.clf()
    mlab.close()

#Test2N1D()
Structures3D()
