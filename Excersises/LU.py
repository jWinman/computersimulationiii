#!/usr/bin/python 
import numpy as np
from scipy import linalg
a = np.array([[2,1,1], [4,-6,0], [-2, 7, 2]])
c = np.array([[1,1,1], [2,2,5], [4, 4, 8]])
lu, p = linalg.lu_factor(c)
b = np.array([5, -2, 9])
x = linalg.lu_solve((lu, p), b, True)
print("c")
print(c)
print("LU")
print(lu)
print("P")
print(p)
print("Solution x")
print(x)

