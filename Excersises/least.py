#!/usr/bin/python 
import numpy as np

x = np.array([1.05, 2.08, 3.01, 4.22])
y = np.array([8.2, 11.1, 14.6, 17.3])
A = np.vstack([x, np.ones(len(x))]).T

m, c = np.linalg.lstsq(A, y)[0]
print m, c

import matplotlib.pyplot as plt
plt.plot(x, y, 'o', label='Original data', markersize=10)
plt.plot(x, m*x + c, 'r', label='Fitted line')
plt.legend()
plt.show()
