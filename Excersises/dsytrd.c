/* Calling Lapack from C */

/* DSYTRD - reduce a real symmetric matrix A to real symmetric tridiagonal form T by an orthogonal similarity transformation */

/* DORGTR - generate a real orthogonal matrix Q, defined as the product of n-1 elementary reflectors, as returned by DSYTRD */

/* DSTEQR - compute all eigenvalues and, optionally, eigenvectors of a symmetric tridiagonal matrix using the implicit QL or QR method */

/* compiled as cc -o dsytrd dsytrd.c -llapack -lblas -lg2c  */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


main()

{

int i, j, info = 0 ;
int m = 4, n = 4, p = 4, k = 4, l ;
int LDA = m, LDU = p, LDZ = m, LWORK = 250 ;
int max ;
char uplo  = 'U' ;
char compz = 'V' ;
double A[LDA][n]  ;
double D[n]  ;
double E[n-1]  ;
double TAU[n-1]  ;
double WORK[LWORK]  ;
double WORK1[2*n-2]  ;

A[0][0] = 1. ;
A[0][1] = 1. ;
A[0][2] = 1. ;
A[0][3] = 1. ;
A[1][0] = 1. ;
A[1][1] = 1. ;
A[1][2] = 1. ;
A[1][3] = 1. ;
A[2][0] = 1. ;
A[2][1] = 1. ;
A[2][2] = 1. ;
A[2][3] = 1. ;
A[3][0] = 1. ;
A[3][1] = 1. ;
A[3][2] = 1. ;
A[3][3] = 1. ;

printf("\nOriginal matrix \n\n") ;
for (i=0;i<m;i++) { for(j=0;j<n;j++) { printf("%10.4lf",A[j][i]) ; } printf("\n") ; } ;
printf("\n\n") ;

dsytrd_(&uplo, &n, A, &LDA, D, E, TAU, WORK, &LWORK, &info) ;

dorgtr_(&uplo, &n, A, &LDA, TAU, WORK, &LWORK, &info) ;

dsteqr_(&compz, &n, D, E, A, &LDZ, WORK1, &info) ;

}
