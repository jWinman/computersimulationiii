#!/usr/bin/python 
import numpy as np

vector = np.identity(13)
q = np.zeros(169).reshape((13,13))
s = np.zeros(169).reshape((13,13))
A = np.loadtxt('basis1.txt')

q[0,0] = 1.0                               # initialise first column of q
for i in range(13):                        # loop over vectors to be normalised
    temp = vector[:,i]
    for j in range(i):                     # loop over projectors
        dot = np.dot(q.T[j,:], np.dot(A, vector[:,i]))
        temp -= dot * q[:,j]               # subtract off projections onto q already used
    mag = np.dot(temp.T, np.dot(A, temp))  
    q[:,i] = temp / np.sqrt(mag)           # normalise current q
            
for i in range(13):
    for j in range(i + 1):
        s[i, j] = np.dot(q.T[i,:], np.dot(A, q[:,j]))

np.set_printoptions(precision = 3, linewidth = 170)
print(q)                                   # orthogonal vectots
print('\n')
print(s)                                   # check norms
