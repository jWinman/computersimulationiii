//     gcc -o LU_decomp1 LU_decomp1.c -lgsl -lgslcblas

     #include <stdio.h>
     #include </usr/local/Cellar/gsl/1.16/include/gsl/gsl_linalg.h>
     
     int
     main (void)
     {

//Function: gsl_matrix * gsl_matrix_alloc (size_t n1, size_t n2)

       int i, j, k, l, s, n1 = 3, out ;

       gsl_matrix *M = gsl_matrix_alloc (n1,n1) ;
       gsl_vector *B = gsl_vector_alloc (n1) ;
       gsl_vector *x = gsl_vector_alloc (n1);
       gsl_permutation *p = gsl_permutation_alloc (n1);

       double a_data[] = { 2.00, 1.00, 1.00,
                           4.00,-6.00, 0.00,
                          -2.00, 7.00, 2.00 } ;
     
       double b_data[] = { 5.00, -2.00, 9.00 };

       k = 0 ; l = 0 ;
       for (i=0;i<n1;i++) {
       for (j=0;j<n1;j++) {
       gsl_matrix_set(M,i,j,a_data[k]) ; 
       k++ ;
                         }
       gsl_vector_set(B,i,b_data[l]) ; 
       l++ ;
                        }
       out = gsl_matrix_fprintf (stdout, M,"M %f") ;
       out = gsl_vector_fprintf (stdout, B,"B %f") ;

       gsl_linalg_LU_decomp (M, p, &s);
     
       gsl_linalg_LU_solve (M, p, B, x);
     
       gsl_vector_fprintf (stdout, x, "X%10.4g");
     
       gsl_permutation_free (p);
       gsl_vector_free (x);
       gsl_vector_free (B);
       gsl_matrix_free (M);

       return 0;
     }


