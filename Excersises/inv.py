#!/usr/bin/python 
import numpy as np
a = np.array([[2,1,1], [4,-6,0], [-2, 7, 2]])
q = np.linalg.inv(a)
print(a)
print('\n')
print(q)
print('\n')
r = np.dot(a, q)
print(r)
