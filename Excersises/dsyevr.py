#!/usr/bin/python
import numpy as np
from scipy import linalg

"""
       DSYEVR  computes  selected eigenvalues and, optionally, eigenvectors of a real symmetric matrix A.  Eigenvalues and eigenvectors can be selected
       by specifying either a range of values or a range of indices for the desired eigenvalues.
       DSYEVR first reduces the matrix A to tridiagonal form T with a call to DSYTRD.  Then, whenever possible, DSYEVR  calls  DSTEMR  to  compute  the
       eigenspectrum  using  Relatively  Robust  Representations.  DSTEMR computes eigenvalues by the dqds algorithm, while orthogonal eigenvectors are
       computed from various "good" L D L^T representations (also known  as  Relatively  Robust  Representations).  Gram-Schmidt  orthogonalization  is
       avoided as far as possible. More specifically, the various steps of the algorithm are as follows.
"""

a = np.array([[2,1,1], [4,2,1.999], [-2, 7, 2]])
b = linalg.lapack.dsyevr(a)
print('\n')
#print(a)
print(b)

