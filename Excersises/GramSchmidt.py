import numpy as np
from scipy.stats import threshold

matrix = np.loadtxt("basis1.txt")

def germanWikipedia(matrix):
    """ Algorithm from the German wikipedia page"""
    q = np.zeros(np.shape(matrix))
    for i in range(len(matrix)):
        bmat = np.zeros(len(matrix))
        for j in range(i):
            bmat += np.dot(q[j], matrix[i]) * q[j]
        q[i] = matrix[i] - bmat
        q[i] /= np.linalg.norm(q[i])
    return q

def mySolution(matrix):
    """ Algorithm following algorithm from script
        Normalization was done with linalg.norm()
    """
    q = np.zeros(np.shape(matrix))
    for i in range(len(matrix)):
        bmat = np.zeros(len(matrix))
        for j in range(i):
            bmat += np.dot(np.outer(q[j], q[j]), matrix[i])
        q[i] = matrix[i] - bmat
        q[i] /= np.linalg.norm(q[i])
    return q


print(np.dot(matrix[0], matrix[1]))

#qGerman = germanWikipedia(matrix)
#test = threshold(np.dot(qGerman.T, qGerman), 1e-14) # threshold to get rid of numerical noise
#
#print("TEST for germanWikipedia():")
#print(test)
#
#q = mySolution(matrix)
#test = threshold(np.dot(q.T, q), 1e-14)
#
#print("TEST for mySolution():")
#print(test)
